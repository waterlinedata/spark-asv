/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.sql.execution.datasources.csv

import java.nio.charset.{Charset, StandardCharsets}

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.FileStatus
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapred.TextInputFormat
import org.apache.hadoop.mapreduce._
import org.apache.spark.TaskContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.catalyst.InternalRow
import org.apache.spark.sql.catalyst.util.CompressionCodecs
import org.apache.spark.sql.execution.datasources._
import org.apache.spark.sql.sources._
import org.apache.spark.sql.types._
import org.apache.spark.util.SerializableConfiguration

/**
 * Provides access to CSV data from pure SQL statements.
 */
class ASVFileFormat extends TextBasedFileFormat with DataSourceRegister {

  override def shortName(): String = "asv"

  override def toString: String = "ASV"

  override def hashCode(): Int = getClass.hashCode()

  override def equals(other: Any): Boolean = other.isInstanceOf[ASVFileFormat]

  override def inferSchema(
      sparkSession: SparkSession,
      options: Map[String, String],
      files: Seq[FileStatus]): Option[StructType] = {

    val asvOptions = new ASVOptions(options)

    // TODO: Move filtering.
    val paths = files.filterNot(_.getPath.getName startsWith "_").map(_.getPath.toString)
    val rdd = baseRdd(sparkSession, asvOptions, paths)

    val firstLine = findFirstLine(asvOptions, rdd)
    val firstRow = new CsvReader(asvOptions).parseLine(firstLine)

    val caseSensitive = sparkSession.sessionState.conf.caseSensitiveAnalysis
    val header = makeSafeHeader(firstRow, asvOptions, caseSensitive)

    val parsedRdd = tokenRdd(rdd, asvOptions, header, firstLine)
    val schema = if (asvOptions.inferSchemaFlag) {
      CSVInferSchema.infer(parsedRdd, header, asvOptions)
    } else {
      // By default fields are assumed to be StringType
      val schemaFields = header.map { fieldName =>
        StructField(fieldName, StringType, nullable = true)
      }
      StructType(schemaFields)
    }
    Some(schema)
  }

  /**
   * Generates a header from the given row which is null-safe and duplicate-safe.
   */
  private def makeSafeHeader(
      row: Array[String],
      options: ASVOptions,
      caseSensitive: Boolean): Array[String] = {
    if (options.headerFlag) {
      val duplicates = {
        val headerNames = row.filter(_ != null)
          .map(name => if (caseSensitive) name else name.toLowerCase)
        headerNames.diff(headerNames.distinct).distinct
      }

      row.zipWithIndex.map { case (value, index) =>
        if (value == null || value.isEmpty || value == options.nullValue) {
          // When there are empty strings or the values set in `nullValue`, put the
          // index as the suffix.
          s"_c$index"
        } else if (!caseSensitive && duplicates.contains(value.toLowerCase)) {
          // When there are case-insensitive duplicates, put the index as the suffix.
          s"$value$index"
        } else if (duplicates.contains(value)) {
          // When there are duplicates, put the index as the suffix.
          s"$value$index"
        } else {
          value
        }
      }
    } else {
      row.zipWithIndex.map { case (_, index) =>
        // Uses default column names, "_c#" where # is its position of fields
        // when header option is disabled.
        s"_c$index"
      }
    }
  }

  override def prepareWrite(
      sparkSession: SparkSession,
      job: Job,
      options: Map[String, String],
      dataSchema: StructType): OutputWriterFactory = {

    verifySchema(dataSchema)
    val conf = job.getConfiguration
    val asvOptions = new ASVOptions(options)
    asvOptions.compressionCodec.foreach { codec =>
      CompressionCodecs.setCodecConfiguration(conf, codec)
    }

    new CSVOutputWriterFactory(asvOptions)
  }

  override def buildReader(
      sparkSession: SparkSession,
      dataSchema: StructType,
      partitionSchema: StructType,
      requiredSchema: StructType,
      filters: Seq[Filter],
      options: Map[String, String],
      hadoopConf: Configuration): (PartitionedFile) => Iterator[InternalRow] = {

    val asvOptions = new ASVOptions(options)
    val commentPrefix = asvOptions.comment.toString

    hadoopConf.set("textinputformat.record.delimiter", asvOptions.lineSeparator)
    val broadcastedHadoopConf =
      sparkSession.sparkContext.broadcast(new SerializableConfiguration(hadoopConf))

    (file: PartitionedFile) => {
      val lineIterator = {
        val conf = broadcastedHadoopConf.value.value
        val linesReader = new HadoopFileLinesReaderAsv(file, conf)
        Option(TaskContext.get()) foreach {_.addTaskCompletionListener {_: TaskContext => linesReader.close()}}
        linesReader.map { line =>
          new String(line.getBytes, 0, line.getLength, asvOptions.charset)
        }
      }

      CSVRelation.dropHeaderLine(file, lineIterator, asvOptions)

      val csvParser = new CsvReader(asvOptions)
      val tokenizedIterator = lineIterator.filter { line =>
        line.trim.nonEmpty && !line.startsWith(commentPrefix)
      }.map { line =>
        csvParser.parseLine(line)
      }
      val parser = CSVRelation.csvParser(dataSchema, requiredSchema.fieldNames, asvOptions)
      var numMalformedRecords = 0
      tokenizedIterator.flatMap { recordTokens =>
        val row = parser(recordTokens, numMalformedRecords)
        if (row.isEmpty) {
          numMalformedRecords += 1
        }
        row
      }
    }
  }

  private def baseRdd(
      sparkSession: SparkSession,
      options: ASVOptions,
      inputPaths: Seq[String]): RDD[String] = {

    val location = inputPaths.mkString(",")
    val sc = sparkSession.sparkContext
    sc.hadoopConfiguration.set("textinputformat.record.delimiter", options.lineSeparator)
    if (Charset.forName(options.charset) == StandardCharsets.UTF_8) {
      sc.textFile(location)
    } else {
      val charset = options.charset
      sc
        .hadoopFile[LongWritable, Text, TextInputFormat](location)
        .mapPartitions(_.map(pair => new String(pair._2.getBytes, 0, pair._2.getLength, charset)))
    }
  }

  private def tokenRdd(
      rdd: RDD[String],
      options: ASVOptions,
      header: Array[String],
      firstLine: String): RDD[Array[String]] = {

    CSVRelation.univocityTokenizer(rdd, if (options.headerFlag) firstLine else null, options)
  }

  /**
   * Returns the first line of the first non-empty file in path
   */
  private def findFirstLine(options: ASVOptions, rdd: RDD[String]): String = {
    if (options.isCommentSet) {
      val comment = options.comment.toString
      rdd.filter { line =>
        line.trim.nonEmpty && !line.startsWith(comment)
      }.first()
    } else {
      rdd.filter { line =>
        line.trim.nonEmpty
      }.first()
    }
  }

  private def verifySchema(schema: StructType): Unit = {
    def verifyType(dataType: DataType): Unit = dataType match {
        case ByteType | ShortType | IntegerType | LongType | FloatType |
             DoubleType | BooleanType | _: DecimalType | TimestampType |
             DateType | StringType =>

        case udt: UserDefinedType[_] => verifyType(udt.sqlType)

        case _ =>
          throw new UnsupportedOperationException(
            s"CSV data source does not support ${dataType.simpleString} data type.")
    }

    schema.foreach(field => verifyType(field.dataType))
  }
}
