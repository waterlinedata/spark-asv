// See http://www.scala-sbt.org/docs/index.html
import sbt.Keys.{fork, libraryDependencies}

// Spark version as this project dependency.
val sparkVersion = "2.1.2"

// Common settings
lazy val commonSettings =
  Seq(organization := "com.waterlinedata",
      version := "1.0.0",
//      licenses := Seq("Apache-2.0" -> url("http://opensource.org/licenses/Apache-2.0")),
      // Scala version must be compatible with Spark Scala version
      scalaVersion := "2.11.12",
      scalacOptions ++= Seq("-deprecation",
                            "-feature",
                            "-unchecked",
                            "-language:_",
                            "-g:vars",
                            "-explaintypes",
                            "-Xlint",
                            "-Xexperimental",
                            "-Xfuture"),
      scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits",
                                             "-doc-external-doc:/usr/java/latest/jre/lib/rt.jar#http://docs.oracle.com/javase/8/docs/api"),
      javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
      // JVM option to use when forking a JVM for 'run'
      javaOptions ++= Seq("-Xmx2G"),
      // Use local repositories if they are needed.
      resolvers ++= Seq(Resolver.defaultLocal,
                        Resolver.mavenLocal,
                        // make sure default maven local repository is added... Resolver.mavenLocal has bugs.
                        "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository",
                        // For Typesafe goodies, if not available through maven
                        // "Typesafe" at "http://repo.typesafe.com/typesafe/releases",
                        // For Spark development versions, if you don't want to build spark yourself
                        "Apache Staging" at "https://repository.apache.org/content/repositories/staging/",
                        // Soft-props retry and odelay
                        "softprops-maven" at "http://dl.bintray.com/content/softprops/maven"),
      // Needed as SBT's classloader doesn't work well with Spark
      fork := true,
      // BUG: unfortunately, it's not supported right now
      fork in console := true,
      libraryDependencies := Seq(),
      assemblyMergeStrategy in assembly := {
        case PathList("META-INF", xs @ _*) => MergeStrategy.discard
        case x => MergeStrategy.first
      },
      assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-assembly-${version.value}.jar")

// Performance reporting tool project
lazy val sparkAsvProject = (project in file("."))
  .settings(commonSettings,
            name := "spark-asv",
            moduleName := s"${name.value}-${sparkVersion}",
            assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
            libraryDependencies := Seq("org.apache.spark" %% "spark-sql" % sparkVersion % Provided,
                                       "org.scalatest" %% "scalatest" % "3.0.2" % Test,
                                       "org.scalacheck" %% "scalacheck" % "1.13.5" % Test,
                                       "junit" % "junit" % "4.12" % Test,
                                       "com.novocode" % "junit-interface" % "0.11" % Test))
